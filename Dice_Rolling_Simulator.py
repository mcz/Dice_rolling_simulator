#!/usr/bin/env python

from random import randint

# Start with a 6 faces dice
userRoll = ''
diceMin = 1
diceMax = 6

def info():
	print('-'  * 60)
	print('----- INFO -----')
	print('the min is {} and the max is {}'.format(diceMin, diceMax))
	print('Press ENTER to roll the dice (default : 1 to 6)')
	print('Write : - masterdice - to change the min and max')
	print('Write : - n - to exit')
	print('Write : - info - to have the info')
	print('Enjoy your game!')
	print('-'  * 60)

print('-'  * 60)
print('welcome to roll dice simulator !')
print('-'  * 60)
info()

print()
while userRoll != 'n':
	
	userRoll = str(input('Roll the dice : '))
	
	if userRoll == '':
		print (randint(diceMin, diceMax))
		userRoll =''
	elif userRoll == 'n':
		print ('-'  * 60)
		print ('Roll Dice Simulator exited')
		print ('See you next time')
		print ('-'  * 60)
	elif userRoll == 'info':
		info()
	elif userRoll == 'masterdice':
		print('what is the min of the dice ?')
		diceMin = int(input())
		print('what is the max of the dice ?')
		diceMax = int(input())
		print ('Your dice have now a min of ' + str(diceMin) + ' and a max of ' + str(diceMax))
	else :
		print ('Please type y for YES and n for quit')
		print ('type info for more information')


